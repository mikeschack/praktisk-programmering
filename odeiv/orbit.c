#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int equation_of_motion(double t, const double u[], double dudt[],void * params)
{	double epsilon = *(double *) params;
	dudt[0]=u[1];
	dudt[1]=1.0-u[0]+epsilon*u[0]*u[0];
	return GSL_SUCCESS;
}



double orbit_motion(double x, double epsilon,double u_0,double dudt_0){
	gsl_odeiv2_system sys;
	sys.function = equation_of_motion;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void *) &epsilon;

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,u[2]={u_0, dudt_0};
	gsl_odeiv2_driver_apply(driver,&t,x,u);

	gsl_odeiv2_driver_free(driver);
	return u[0];
}



int main(){
	double v_i=0, v_f=20*2*M_PI,dx=0.01;
	for(double x=v_i;x<=v_f;x+=dx)printf("%g %g %g %g %g %g\n",1/orbit_motion(x,0,1,0)*cos(x),1/orbit_motion(x,0,1,0)*sin(x),1/orbit_motion(x,0,1,-0.5)*cos(x),1/orbit_motion(x,0,1,-0.5)*sin(x),1/orbit_motion(x,0.01,1,-0.5)*cos(x),1/orbit_motion(x,0.01,1,-0.5)*sin(x));
	
	return 0;
}
	
