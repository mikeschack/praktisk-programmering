#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int erf_diff_eq(double t, const double y[], double dydt[], void * params)
{
	dydt[0]=2/sqrt(M_PI)*exp(-t*t);
	return GSL_SUCCESS;
}



double error_fun(double x){
	gsl_odeiv2_system sys;
	sys.function = erf_diff_eq;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,y[1]={0};
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(int argc, char** argv){

	double x_i=atof(argv[1]), x_f=atof(argv[2]), dx=atof(argv[3]);	

	for(double x=x_i;x<=x_f;x+=dx)printf("%g %g %g\n",x,error_fun(x),erf(x));
	
	return 0;
}	

