#include <stdio.h>
#include "komplex.h"

void komplex_print(char* s, komplex z){
	double x=z.re;
	double y=z.im;	
	
	printf("%s",s);
	printf("%g + %g i\n",x,y);	
}

void komplex_set(komplex* z, double x, double y){
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_new(double x, double y){
	komplex z;

	z.re = x;
	z.im = y;

	return z;
}

komplex komplex_add(komplex a, komplex b){
	komplex z;

	z.re=a.re+b.re;
	z.im=a.im+b.im;
	
	return z;
}

komplex komplex_sub(komplex a, komplex b){
	komplex z;

	z.re=a.re-b.re;
	z.im=a.im-b.im;
	
	return z;
}


