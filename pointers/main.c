#include<stdio.h>
#include"komplex.h"

int main(){
	
	komplex v1 = {0.8,2.54}, v2 = {1.739,4.20};
	
	komplex add_is = komplex_add(v1,v2);
	komplex add_exp = {2.539,6.74};
	komplex sub_is = komplex_sub(v1,v2);
	komplex sub_exp = {-0.939,-1.66};
	
	komplex_print("v1 =",v1);
	komplex_print("v2 =",v2);
	komplex_print("Should be: v1 + v2 = ", add_exp);
	komplex_print("Actually is: v1 + v2 = ", add_is);
	
	komplex_print("Should be: v1 - v2 = ", sub_exp);
	komplex_print("Actually is: v1 - v2 = ", sub_is);
	
	return 0;
}
