#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double f(double x,void * params) {
	double f = log(x) / sqrt(x);
	
	return f;
}

int main(){
	int n=1000;
	gsl_integration_workspace * w=gsl_integration_workspace_alloc (n);
  	double result, error;
	
  	gsl_function F;
  	F.function = &f;
	F.params=NULL;

  	gsl_integration_qags (&F, 0, 1, 1e-6, 1e-6, n, w, &result, &error);
	printf("Integral in first part gives: %g \n",result);
	
	gsl_integration_workspace_free (w);
	return 0;
}
