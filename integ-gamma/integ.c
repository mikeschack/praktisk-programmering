#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double prob_fun(double x,void * params) {
	double alpha=*(double*)params;	
	double prob_fun = exp(-alpha*x*x);
	
	return prob_fun;
}


double psi_H_psi(double x, void * params){
	double alpha=*(double*)params;	
	double psi_H_psi =(-pow(alpha*x,2)/2+alpha/2+pow(x,2)/2)*exp(-alpha*x*x);
	
	return psi_H_psi;
}


double E(double alpha){
	int n=1000;

	gsl_integration_workspace * w1=gsl_integration_workspace_alloc (n);
  	double result1, error1;
	
  	gsl_function F1;
  	F1.function = &prob_fun;
	F1.params=(void*)&alpha;

  	gsl_integration_qagi(&F1, 1e-6, 1e-6, n, w1, &result1, &error1); 
	
	gsl_integration_workspace * w2=gsl_integration_workspace_alloc (n);
	double result2, error2;
	
	gsl_function F2;
  	F2.function = &psi_H_psi;
	F2.params=(void*)&alpha;

	gsl_integration_qagi(&F2, 1e-6, 1e-6, n, w2, &result2, &error2); 

	gsl_integration_workspace_free (w1);
	gsl_integration_workspace_free (w2);
	
	return result2/result1;
}


int main(){
	for(double alpha=0.1; alpha<10; alpha+=0.01){ 
	printf("%g %g\n",alpha,E(alpha));}
	
	return 0;
}

