#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_vector.h>

double *grad_Rosenbrock(double x, double y){
	double *grad=malloc(2*sizeof(double));		
	grad[0]=2*(200*x*(x*x-y)+x-1);
	grad[1]=200*(y-x*x);			
	
	return grad; 	
	free(grad);	
}

int master(const gsl_vector * z, void * params, gsl_vector * f){
	double x = gsl_vector_get(z,0);
	double y = gsl_vector_get(z,1);
	double *grad=grad_Rosenbrock(x, y);

	gsl_vector_set(f,0,grad[0]);	
	gsl_vector_set(f,1,grad[1]);	
	return GSL_SUCCESS;
}

int main(){

const int dim = 2;
gsl_multiroot_function M;
M.f = master;
M.n = dim;
M.params = NULL;
gsl_multiroot_fsolver * s = gsl_multiroot_fsolver_alloc (gsl_multiroot_fsolver_hybrids, dim);
gsl_vector * x = gsl_vector_alloc(dim);
gsl_vector_set(x,0,0.9);
gsl_vector_set(x,1,0.9);

gsl_multiroot_fsolver_set (s, &M, x);

int iter=0,status;
const double acc=1e-3;
do{
	iter++;
	int flag=gsl_multiroot_fsolver_iterate(s);
	if(flag!=0)break;
	status = gsl_multiroot_test_residual (s->f, acc);
	fprintf(stderr
		,"iter= %2i, Grad(Rosenbrock)(%8g,%8g) = (%8g,%8g) \n"
		,iter
		,gsl_vector_get(s->x,0)
		,gsl_vector_get(s->x,1)
		,gsl_vector_get(s->f,0)
		,gsl_vector_get(s->f,1)
		);
}while(status == GSL_CONTINUE && iter<99);
	
gsl_vector_free(x);
gsl_multiroot_fsolver_free (s);
return 0;
}
