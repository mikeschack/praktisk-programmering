#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

double fit_fun(double A, double B, double T){

	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);
	
	double F=0;
	for(int i=0;i<n;i++){
	F+=pow((A*exp(-t[i]/T)+B-y[i])/e[i],2);
	}

	return F;
}

double master (const gsl_vector * x, void * params){
	double A=gsl_vector_get(x,0);
	double B=gsl_vector_get(x,1);
	double T=gsl_vector_get(x,2);	

	return fit_fun(A,B,T);
}

int main(){
	size_t dim=3;
	gsl_multimin_function F;
	F.f=master;
	F.n=dim;
	F.params=NULL;

	gsl_multimin_fminimizer * state =
	gsl_multimin_fminimizer_alloc (gsl_multimin_fminimizer_nmsimplex2,dim);
	gsl_vector *start = gsl_vector_alloc(dim);
	gsl_vector *step = gsl_vector_alloc(dim);
	gsl_vector_set(start,0,5); /* A_start */
	gsl_vector_set(start,1,1); /* B_start */
	gsl_vector_set(start,2,3); /* T_start */
		
	gsl_vector_set_all(step,0.05);
	gsl_multimin_fminimizer_set (state, &F, start, step);

	int iter=0,status;
	double acc=0.001;
	do{
		iter++;
		int flag = gsl_multimin_fminimizer_iterate (state);
		if(flag!=0)break;
		status = gsl_multimin_test_size (state->size, acc);
		if (status == GSL_SUCCESS)printf ("converged\n");
		printf("iter=%2i, A= %8g, B= %8g, T=%8g, Fit_fun= %8g, size= %8g\n",
			iter,
			gsl_vector_get(state->x,0),
			gsl_vector_get(state->x,1),
			gsl_vector_get(state->x,2),
			state->fval,
			state->size);
	}while(status == GSL_CONTINUE && iter < 99);

	double A=gsl_vector_get(state->x,0), B=gsl_vector_get(state->x,1), T=gsl_vector_get(state->x,2);

	fprintf(stderr,"%g %g %g\n",A,B,T);

	gsl_vector_free(start);
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(state);
	return 0;
}
