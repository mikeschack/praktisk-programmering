#include <stdio.h>
#include <limits.h>
#include <float.h>
#include <math.h>

int equal(); /*equal epsilon function*/

void ex1(){
printf("INT_MAX := %i\n",INT_MAX);

int i=1; while(i+1>i) {i++;};

printf("while: %i\n",i);

int j; for(j=1;j<j+1;j++){};

printf("for: %i\n",j);

int k=1;
do{k++;}
while(k<k+1);

printf("do while: %i\n\n",k);

printf("DBL_EPSILON := %g\n",DBL_EPSILON);

double x=1; while(1+x!=1){x/=2;} x*=2;

printf("while: %g\n",x);

double y; for(y=1; y+1!=1; y/=2){};
y*=2;

printf("for: %g\n",y);

double z=1; do{z/=2;} while(z+1!=1);
z*=2;
printf("do while: %g\n\n",z);

}

void ex2(){

int max=INT_MAX;

int lf=0;

float suf=0; while(lf!=max){suf+=1.0/(lf+1),lf++;};

printf("sum up w. float = %g \n",suf);

int mf=0;
	
float sdf=0; while(mf!=max){sdf+=1.0/(max-mf),mf++;};

printf("sum down w. float =%g \n",sdf);

int ld=0;

double sud=0; while(ld!=max){sud+=1.0/(ld+1),ld++;};

printf("sum up w. double = %g \n",sud);

int md=0;
	
double sdd=0; while(md!=max){sdd+=1.0/(max-md),md++;};

printf("sum down w. double = %g \n\n",sdd);

}


int main(){

ex1();

ex2();

double a=2.0, b=1.5, eps=0.5, tau=1.0;

int rval=equal(a,b,eps,tau);
printf("%g = %g w. abs. prec. %g and rel. prec. %g if %i = %i \n",a,b,tau,eps,1,rval);


}













