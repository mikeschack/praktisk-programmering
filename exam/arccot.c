#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int arccot_diff_eq(double t, const double y[], double dydt[], void * params)
{
	dydt[0]=-1/(t*t+1);
	return GSL_SUCCESS;
}

double arccot_fun(double x){

	if(x<0)return -arccot_fun(-x);

	gsl_odeiv2_system sys;
	sys.function = arccot_diff_eq;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,y[1]={M_PI/2};
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(){

	double x_i=-3,x_f=3,dx=0.1;
	for(double x=x_i;x<=x_f;x+=dx)printf("%g %g %g\n",x,arccot_fun(x),atan(1/x));

}




