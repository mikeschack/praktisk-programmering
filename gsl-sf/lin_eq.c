#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_linalg.h>

int main(){

double tmp1[]={6.13,-2.90,5.86,8.08,-6.31,-3.89,-4.36,1.00,0.19};
double tmp2[]={6.23,5.37,2.29};

gsl_matrix *A = gsl_matrix_calloc(3,3);
gsl_vector *b = gsl_vector_calloc(3);
gsl_vector *x = gsl_vector_calloc(3);

for(int i=0;i<3;i++){
	for(int j=0;j<3;j++){
		gsl_matrix_set(A,i,j,tmp1[3*i+j]);
	};
};

for(int k=0;k<3;k++){
	gsl_vector_set(b,k,tmp2[k]);
};

gsl_linalg_HH_solve(A,b,x);

printf("\n\n Solution to Ax=b: \n x = \n");

gsl_vector_fprintf(stdout,x," %g");

return 0;
}
