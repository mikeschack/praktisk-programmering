#include<stdio.h>
#include<stdlib.h>
#include<stdlib.h>
#include <time.h>
#include"nvector.h"
#define RND (double)rand()/RAND_MAX

int main(){

	srand((unsigned) time(NULL));

	int n=5;
	
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x * y);
		}

	double dot_prod=nvector_dot_product(a, b);
	
	double real_val=0;

	for (int i = 0; i < n; i++) {real_val+=nvector_get(c,i);	
		}
	printf("Inner product gives %g\n",dot_prod);
	printf("Inner product should give %g\n",real_val);
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);


return 0;
}
