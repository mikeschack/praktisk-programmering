#include<stdio.h>
#include<stdlib.h>
#include"nvector.h"

nvector* nvector_alloc(int n){
	if( n==0 ) fprintf(stderr,"Error in nvector_alloc. Vector size is 0.\n");	
	nvector* av = malloc(sizeof(nvector));
	av->size = n;
	av->data = malloc(n*sizeof(double));
	return av;
}

void nvector_free(nvector* v){
	free(v->data);
	free(v);
}

void nvector_set(nvector* v, int i, double value){
	int n=v->size;

	if( n==0 ) fprintf(stderr,"Error in nvector_set. Vector size is 0.\n");	
	if( n<i ) fprintf(stderr,"Error in nvector_set. Index higher than size.\n");
	
	v->data[i]=value;
}

double nvector_get(nvector* v, int i){
	int n=v->size;

	if( n==0 ) fprintf(stderr,"Error in nvector_get. Vector size is 0.\n");	
	if( n<i ) fprintf(stderr,"Error in nvector_get. Index higher than size.\n");
		
	return v->data[i];
}

double nvector_dot_product(nvector* u, nvector* v){
	int n1=u->size;
	int n2=v->size;
	
	if( n1==0) fprintf(stderr,"Error in nvector_get. First vector size is 0.\n");
	if( n2==0) fprintf(stderr,"Error in nvector_get. Second vector size is 0.\n");
	if(n1!=n2) fprintf(stderr,"Error in nvector_get. Vector sizes not equal.\n");
	
	double dot_prod=0;
	for(int i=0; i<n1; i++){dot_prod+=(u->data[i])*(v->data[i]);};
	return dot_prod;
}


