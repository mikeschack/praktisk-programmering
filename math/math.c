#include<stdio.h>
#include<tgmath.h>

int main(){

double x=5;
double gx=tgamma(x);
printf("gamma(%g) = %g \n",x,gx);

double y=0.5;
double by=j0(y);
printf("Bessel1(%g) = %g \n",y,by);

double complex s=csqrt(-2.0);
double sr=creal(s);
double si=cimag(s);
printf("sqrt(-2) = %g + i %g \n",sr,si);

double complex eipi=cpow(M_E,I*M_PI);
double eipir=creal(eipi);
double eipii=cimag(eipi);
printf("e^(i pi) = %g + i %g \n",eipir,eipii);

double complex ei=cpow(M_E,I);
double eir=creal(ei);
double eii=cimag(ei);
printf("e^i = %g + i %g \n",eir,eii);

double complex ie=cpow(I,M_E);
double ier=creal(ie);
double iei=cimag(ie);
printf("i^e = %g + i %g \n",ier,iei);

double js=0.111111111111111111111111111111111111111111111111111111111L;
long double ljs=0.111111111111111111111111111111111111111111111111111L;
float hb=0.111111111111111111111111111111111111111111111111111L;
printf("Long double John silver: %.25Lg \nDouble: %.25lg \nEvery boats hobby: %.25g\n",ljs,js,hb);
}
